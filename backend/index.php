<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: https://scandit-dff64.web.app/");
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE");
// header("Access-Control-Allow-Origin: http://localhost:3000/");


// echo "Testing";

include 'DbConnect.php';
include 'Logic.php';

$objDb = new DbConnect;
$conn = $objDb->connect();
$logic = new Logic($conn);
// var_dump($conn);
$product = file_get_contents('php://input');
print_r($product);
$method = $_SERVER['REQUEST_METHOD'];
switch ($method) {

  case "DELETE":
    $logic->delete();
    break;

  case "GET":
    $sql = "SELECT * FROM products2";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($products);
    break;

  case "POST":
    $logic->post();
    break;
}

?>
