<?php

// $conn = new PDO("mysql:host=$this->server;dbname=$this->dbname, $this->user, $this->pass");
// $conn = new PDO('mysql:host='.$this->server.';dbname='.$this->dbname, $this->user, $this->pass);

class DbConnect {
  private $server = "localhost";
  // private $server = "localhost";
  private $dbname = "dbname";
  // private $dbname = "scandiweb";
  private $user = "username";
  // private $user = "root";
  private $pass = "password";
  // private $pass = "";

  public function connect() {
    try {
      $conn = new PDO('mysql:host='.$this->server.';dbname='.$this->dbname, $this->user, $this->pass);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $conn;
    } catch (\Exception $e) {
        echo "Database Error: " . $e->getMessage();
    }
  }
}
?>
