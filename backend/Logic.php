<?php


class Logic {

  public function __construct($conn) {
    $this->conn = $conn;
  }

  public function delete() {
    $sql = "DELETE FROM products2 WHERE SKU=:item ORDER BY SKU";
    $path = explode('/', $_SERVER['REQUEST_URI']);
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':item', $path[3]);
    $stmt->execute();
    if ($stmt->execute()) {
      $response = ['status' => 1, 'message' => 'Deleted successfully'];
    } else {
      $response = ['status' => 0, 'message' => 'Failed to Delete record'];
    }
  }

  public function post() {
    $product = json_decode(file_get_contents('php://input'));
    $sql = 'INSERT INTO products2(SKU, Selected,Name, Price, Size, Weight, Width, Height, Length) VALUES (:sku, :selected,:name, :price, :size, :weight, :width, :height, :length)';
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':sku', $product->sku);
    $stmt->bindParam(':selected', $product->selected);
    $stmt->bindParam(':name', $product->name);
    $stmt->bindParam(':price', $product->price);
    $stmt->bindParam(':size', $product->size);
    $stmt->bindParam(':weight', $product->weight);
    $stmt->bindParam(':height', $product->height);
    $stmt->bindParam(':width', $product->width);
    $stmt->bindParam(':length', $product->length);
    if ($stmt->execute()) {
      $response = ['status' => 1, 'message' => 'Record created successfully'];
    } else {
      $response = ['status' => 0, 'message' => 'Failed to save record'];
    }
    echo json_encode($response);
  }
}
?>
